#!/bin/bash
#build in jenkins 
REG_URL=192.168.135.130:5000
SWARM_MANAGE_URL=192.168.135.130:2375
TAG=$REG_URL/$JOB_NAME:`date +%y%m%d-%H-%M`
docker run --rm --name mvn1  -v /mnt/maven:/root/.m2 -v /mnt/jenkins_home/workspace/$JOB_NAME:/usr/src/mvn -w /usr/src/mvn/ maven:3.3.3-jdk-8 mvn clean install -Dmaven.test.skip=true
docker build -t  $TAG  $WORKSPACE/.
docker push   $TAG
docker rmi $TAG

if docker -H $SWARM_MANAGE_URL ps -a| grep -i $JOB_NAME; then
        docker -H $SWARM_MANAGE_URL rm -f  $JOB_NAME
 fi
docker -H $SWARM_MANAGE_URL pull  $TAG 
docker -H $SWARM_MANAGE_URL run  -d  -p 8080:8080  --name $JOB_NAME  $TAG 


 